from django.db import models
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import AbstractUser


class TypeDocument(models.Model):
    title = models.CharField(max_length=50, verbose_name='Тип документа')


class Position(models.Model):
    title = models.CharField(max_length=50, default=None, verbose_name='Должность')


class Subdivision(models.Model):
    title = models.CharField(verbose_name='Название подразделения', max_length=100)


class Document(models.Model):
    title = models.CharField(max_length=100)
    type_document = models.ForeignKey(TypeDocument, null=True, on_delete = models.SET_NULL, verbose_name='Тип документа')
    date = models.DateTimeField(null = True, blank = True, default = timezone.now, verbose_name='Дата загрузки')
    description = models.TextField(default = '', blank = True, verbose_name='Описание документа')
    file = models.FileField()


class User(AbstractUser):
    subdivision_name = models.ForeignKey(Subdivision, null=True, on_delete = models.SET_NULL)
    position_name = models.ForeignKey(Position, null=True, on_delete = models.SET_NULL, verbose_name='Должность')
    # avatar = models.CharField(max_length=200)
    first_name = models.CharField(max_length=100, null = True, default=None, verbose_name='Имя')
    last_name = models.CharField(max_length=100, null = True, default=None, verbose_name='Фамилия')
    second_name = models.CharField(max_length=100, null = True, default=None, verbose_name='Отчество')
    phone = models.CharField(max_length=20, null = True, default=None, verbose_name='Телефон')

# class Action(models.Model):
#     title = models.CharField(max_length=150, verbose_name='Название действия')


class Comment(models.Model):
    document = models.ForeignKey(Document, related_name='comments', on_delete=models.CASCADE, verbose_name='Документ')
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, verbose_name='Сотрудник')
    text = models.TextField(verbose_name="Текст комментария")
    date = models.DateField(blank=True, default=timezone.now, verbose_name='Дата')


class Branch(models.Model):
    title = models.CharField(max_length=150, verbose_name='Название ветки')
    document = models.ForeignKey(Document, null=True, related_name='Документ', on_delete=models.SET_NULL)
    status = models.BooleanField(default=False)


class BranchPoint(models.Model):
    branch = models.ForeignKey(Branch, verbose_name='Ветвь', on_delete=models.CASCADE)
    point_status = models.BooleanField(default=False)


class UserPointStatus(models.Model):
    user = models.ForeignKey(User, null=True, verbose_name='Сотрудник', on_delete=models.SET_NULL)
    point = models.ForeignKey(BranchPoint, null=True, related_name='all_users', verbose_name='Контрольная точка', on_delete=models.CASCADE)
    status = models.BooleanField(default=False, verbose_name='Статус')






