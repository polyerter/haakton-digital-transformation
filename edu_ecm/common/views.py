from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import login, logout, authenticate

from . import forms as common_forms


class Main(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('login')
        return render(request, 'index.html')


class Login(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('main-page')
        form = common_forms.LoginForm()
        return render(request, '/../templates/registrations/login.html', {'login_form': form})

    def post(self, request, *args, **kwargs):
        form = common_forms.LoginForm(request.POST or None)
        if form.is_valid():
            user = authenticate(request, username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            login(request, user)
            return redirect('main-page')
        return render(request, 'login.html', {'login_form': form})


class Logout(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            logout(self.request)
        return redirect('login')

class Create(View):

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('main-page')
        return render(request, 'create.html')

class Profile(View):
     def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('main-page')
        return render(request, 'registration/profile.html')