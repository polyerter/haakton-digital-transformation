from django.contrib import admin
from .models import TypeDocument, Position, Subdivision, Document, User, Comment, Branch, BranchPoint, UserPointStatus

admin.site.register(TypeDocument)
class Meta:
    verbose_name = 'Тип документа'
    verbose_name_plural = 'Типы документов'


admin.site.register(Position)
class Meta:
    verbose_name = 'Должность'
    verbose_name_plural = 'Должности'


admin.site.register(Subdivision)
class Meta:
    verbose_name = 'Подразделение'
    verbose_name_plural = 'Подразделения'


admin.site.register(Document)
class Meta:
    verbose_name = 'Документ'
    verbose_name_plural = 'Документы'


admin.site.register(User)
class Meta:
    verbose_name = 'Сотрудник'
    verbose_name_plural = 'Сотрудники'


admin.site.register(Comment)
class Meta:
    verbose_name = 'Комментарий'
    verbose_name_plural = 'Комментарии'


admin.site.register(Branch)
class Meta:
    verbose_name = 'Ветка'
    verbose_name_plural = 'Ветки'


admin.site.register(BranchPoint)
class Meta:
    verbose_name = 'Контрольная точка'
    verbose_name_plural = 'Контрольные точки'


admin.site.register(UserPointStatus)
class Meta:
    verbose_name = 'Статус сотрудника в точке'
    verbose_name_plural = 'Статусы сотрудников в точках'