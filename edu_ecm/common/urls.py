from django.urls import path
from . import views


urlpatterns = [
    path('', views.Main.as_view(), name='main-page'),
    path('create/', views.Create.as_view(), name='create'),
    path('login/', views.Login.as_view(), name='login'),
    path('logout/', views.Logout.as_view(), name='logout'),
    path('accounts/profile/', views.Profile.as_view(), name = 'profile')
]
