from django import forms
from django.contrib.auth import authenticate
from .models import TypeDocument, Position, Document, User


class LoginForm(forms.ModelForm):
    username = forms.CharField(max_length=30, label='Логин', widget=forms.TextInput())
    password = forms.CharField(max_length=30, label='Пароль', widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'password']

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        if not authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password']):
            raise forms.ValidationError('Неверный логин или пароль')
        return self.cleaned_data

